"""DjangoShizhan URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from app01 import views
from django.urls import path, re_path, include
from django.views.static import serve
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
                  path('login/', views.login),
                  path('logout/', views.logout),
                  path('register/', views.register),
                  path('userinfo/', views.userinfo),
                  path('userinfo/delete/', views.delete_user),
                  path('userinfo/details/', views.userinfo_detail),
                  path('user/add/', views.user_add),
                  path('userinfo/edit/', views.userinfo_edit),
                  path('testcase/', views.testcase),
                  path('testcase/add/', views.testcase_add),
                  path('testcase/edit/', views.testcase_edit),
                  path('testcase/<int:deid>/delete/', views.testcase_delete),
                  path('project/', views.project_info),
                  path('project/add/', views.project_add),
                  path('project/edit/', views.project_edit),
                  path('project/<int:deid>/delete/', views.project_delete),
                  path('module/', views.project_info),
                  path('module/add/', views.module_add),
                  path('module/edit/', views.module_edit),
                  path('module/<int:deid>/delete/', views.module_delete),
                  path('dialog/',views.dialog),
                  path('json/', views.test_json),

              ] + [
                  path('admin/', admin.site.urls),
                  re_path(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
