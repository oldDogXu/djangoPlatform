import json

from django.db import models


class JSONField(models.TextField):
    __metaclass__ = models.Subquery
    description = "Json"

    def to_python(self, value):
        v = models.TextField.to_python(self, value)
        try:
            return json.loads(v)['v']
        except:
            pass
        return v

    def get_prep_value(self, value):
        return json.dumps({'v': value})


# Create your models here.
class User(models.Model):
    """用户表"""
    account = models.CharField(verbose_name="账号", max_length=32, default=None)
    password = models.CharField(verbose_name="密码", max_length=32, default=None)
    email = models.EmailField(verbose_name="邮箱")
    create_time = models.DateTimeField(verbose_name="创建时间", default=None)
    sex = models.IntegerField(verbose_name="性别", default=1, choices=[(1, "男"), (2, "女")])
    avatar = models.ImageField(verbose_name="头像", blank=True, null=True, upload_to='images/user')
    balance = models.DecimalField(verbose_name="额度", default=0, max_digits=12, decimal_places=2)

    def __str__(self):
        return self.sex


class UserInfo(models.Model):
    """用户信息表"""
    account = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="账号", max_length=20, null=True)
    balance = models.DecimalField(verbose_name="额度", default=0, max_digits=12, decimal_places=2, null=True)


class Project(models.Model):
    """项目"""
    project_name = models.CharField(verbose_name="项目名称", max_length=20, default=None)

    def __str__(self):
        return self.project_name


class Modules(models.Model):
    """模块"""
    project_name = models.ForeignKey(Project, on_delete=models.CASCADE, verbose_name="模块", max_length=32,
                                     default=None)
    module_name = models.CharField(verbose_name="模块", max_length=32, default=None)

    def __str__(self):
        return self.module_name


class Testcase(models.Model):
    """测试用例"""
    project_name = models.ForeignKey(Project, on_delete=models.CASCADE, verbose_name="项目", max_length=32,
                                     default=None)
    module_name = models.ForeignKey(Modules, on_delete=models.CASCADE, verbose_name="模块", max_length=32, default=None)
    title = models.CharField(verbose_name="标题", max_length=50, default=None)
    params = models.JSONField(verbose_name="用例参数", default=dict)
    expect_result = models.JSONField(verbose_name="预期结果", default=dict)
