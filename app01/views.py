from django.shortcuts import render, redirect, HttpResponse
from app01 import models
from datetime import datetime
from django.forms import ModelForm


# Create your views here.

def public_page_num(request):
    """实现分页"""
    page = int(request.GET.get("page", 1))
    next_page = page + 1
    if page > 1:
        last_page = page - 1
    else:
        last_page = page = 1
    start_index = (page - 1) * 10
    end_index = page * 10
    return page, last_page, next_page, start_index, end_index


def register(request):
    """注册"""
    if request.method == "GET":
        return render(request, 'register.html')

    username = request.POST.get('user')
    pwd = request.POST.get('pwd')
    sex = request.POST.get("sex")
    if username and pwd and sex:
        db_user = models.User.objects.filter(account=username).first()
        print("db_user", db_user)
        if db_user is None:
            create_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            models.User.objects.create(account=username, password=pwd, create_time=create_time, sex=sex)
            return redirect("/login/")
        else:
            return render(request, 'register.html', {"error": "{}用户名已被注册".format(username)})
    else:
        return render(request, 'register.html', {"error": "注册用户名,性别或密码不能为空"})


def login(request):
    """登录"""
    if request.method == "GET":
        return render(request, 'login.html')

    username = request.POST.get('user')
    pwd = request.POST.get('pwd')
    print(request.method)
    print(username, pwd)
    if username and pwd:
        db_user = models.User.objects.filter(account=username, password=pwd).first()
        print("db_user", db_user)
        if db_user:
            request.session["user_session"] = {"id": db_user.id, "account": db_user.account}
            return redirect('/userinfo/')
        else:
            return render(request, 'login.html', {"error": "Sorry，'{}'用户名或密码不正确！".format(username)})
    else:
        return render(request, 'login.html', {"error": "用户名或密码不能空"})


def logout(request):
    """注销"""
    request.session.clear()
    return redirect('/login/')


class UserModelForm(ModelForm):
    class Meta:
        model = models.User
        fields = ['sex', 'account', 'password', 'email', 'balance']
        # fields = "__all__"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name, field in self.fields.items():
            field.widget.attrs["class"] = "form-control"


def userinfo(request):
    """用户列表"""
    """返回列表并实现分页
    字段名__contains，不区分大小写
    """
    data_dict = {}
    key = request.GET.get("key", "")
    email = request.GET.get("email", "")
    result = public_page_num(request)
    total = models.User.objects.all().order_by('-id').count()
    if key or email:
        data_dict['account__contains'] = key.strip()
        data_dict['email__contains'] = email.strip()
        user_list = models.User.objects.filter(**data_dict).order_by("-id")[result[3]:result[4]]
    else:
        user_list = models.User.objects.all().order_by("-id")[result[3]:result[4]]

    return render(request, 'userlist.html',
                  {"user_list": user_list, "total": total, "last_page": result[1], "next_page": result[2],
                   "page": result[0], "url": "/userinfo/"})


def delete_user(request):
    """删除用户"""
    deid = request.GET.get('deid')
    models.User.objects.filter(id=deid).delete()
    return redirect('/userinfo/')


def userinfo_detail(request):
    userid = request.GET.get("userid")
    user_obj = models.User.objects.filter(id=userid).first()
    return render(request, 'user_details.html', {"user_obj": user_obj})


def userinfo_edit(request):
    """
        if request.method == "GET":
        userid = request.GET.get("userid")
        user_obj = models.User.objects.filter(id=userid).first()
        return render(request, 'user_edit.html', {"user_obj": user_obj})
    else:
        userid = request.GET.get("userid")
        new_password = request.POST.get("pwd")
        print("用户编辑：userid:{}, new_password;{}".format(userid, new_password))
        if not new_password:
            return render(request, 'user_edit.html', {"error": "新密码不可为空"})
        elif new_password != models.User.objects.filter(id=userid).first().password:
            models.User.objects.filter(id=userid).update(password=new_password)
            return redirect('/userinfo/')

    :param request:
    :return:
    """
    title = "用户编辑"
    if request.method == "GET":
        user_id = request.GET.get('id')
        user_obj = models.User.objects.filter(id=user_id).first()
        model_form = UserModelForm(instance=user_obj)
        return render(request, "public_edit.html", {"model_form": model_form, "url": "/userinfo/","title": title})
    else:
        user_id = request.GET.get('id')
        user_obj = models.User.objects.filter(id=user_id).first()
        model_form = UserModelForm(data=request.POST, instance=user_obj)
        """表单验证"""
        if model_form.is_valid():
            model_form.save()
            return redirect('/userinfo/')
        else:
            return render(request, 'public_edit.html', {"model_form": model_form, "title": title})


def user_add(request):
    """添加用户"""
    # if request.method == "GET":
    #     return render(request, 'user_add.html')
    # account = request.POST.get("account")
    # password = request.POST.get("pwd")
    # create_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    # models.User.objects.create(account=account, password=password, create_time=create_time)
    # return redirect('/userinfo/')
    title = "添加用户"
    if request.method == "GET":
        model_form = UserModelForm()
        return render(request, "public_add.html", {"model_form": model_form, "title": title, "url": "/userinfo/"})
    else:
        model_form = UserModelForm(data=request.POST)
        if model_form.is_valid():
            print(model_form.cleaned_data)
            model_form.save()
            return redirect("/userinfo/")
        else:
            return render(request, "public_add.html", {"model_form": model_form, "title": title})


def testcase(request):
    """返回分页用例列表"""
    result = public_page_num(request)
    case_obj = models.Testcase.objects.all().order_by('-id')[result[3]:result[4]]
    total = models.Testcase.objects.all().order_by('-id').count()
    return render(request, 'testcaselist.html',
                  {"case_obj": case_obj, "total": total, "last_page": result[1], "next_page": result[2],
                   "page": result[0], "url": "/testcase/"})


class TestcaseModelForm(ModelForm):
    class Meta:
        model = models.Testcase
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name, field in self.fields.items():
            field.widget.attrs["class"] = "form-control"


def testcase_add(request):
    title = "添加用例"
    if request.method == "GET":
        # for i in range(100):
        #     models.Testcase.objects.create(title=i,project_name_id=1,params={"text":"test"+str(i)},expect_result={"text":"test"+str(i)},module_name_id=1)
        model_form = TestcaseModelForm()
        return render(request, "public_add.html", {"model_form": model_form, "title": title, "url": "/testcase/"})
    else:
        model_form = TestcaseModelForm(data=request.POST)
        if model_form.is_valid():
            print(model_form.cleaned_data)
            model_form.save()
            return redirect("/testcase/")
        else:
            return render(request, "public_add.html", {"model_form": model_form, "title": title})


def testcase_edit(request):
    """用例编辑"""
    """ModelForm给实例对象提供instance，会将编辑页面的表单赋值"""
    title = "用例编辑"
    if request.method == "GET":
        case_id = request.GET.get('id')
        case_obj = models.Testcase.objects.filter(id=case_id).first()
        model_form = TestcaseModelForm(instance=case_obj)
        return render(request, "public_edit.html", {"model_form": model_form, "url": "/testcase/", "title": title})
    else:
        case_id = request.GET.get('id')
        case_obj = models.Testcase.objects.filter(id=case_id).first()
        model_form = TestcaseModelForm(data=request.POST, instance=case_obj)
        """表单验证"""
        if model_form.is_valid():
            model_form.save()
            return redirect('/testcase/')
        else:
            return render(request, 'public_edit.html', {"model_form": model_form, "title": title})


def testcase_delete(request, deid):
    # deid = request.GET.get('deid')
    models.Testcase.objects.filter(id=deid).delete()
    return redirect('/testcase/')


def project_info(request):
    """返回分页用例列表"""
    result = public_page_num(request)
    case_obj = models.Project.objects.all().order_by('-id')[result[3]:result[4]]
    total = models.Project.objects.all().order_by('-id').count()
    return render(request, 'project_list.html',
                  {"case_obj": case_obj, "total": total, "last_page": result[1], "next_page": result[2],
                   "page": result[0], "url": "/project/"})


def project_add(request):
    pass


def project_edit(request):
    pass


def project_delete(request):
    pass


def module_info(request):
    pass


def module_add(request):
    pass


def module_edit(reqeust):
    pass


def module_delete(request):
    pass


def dialog(request):
    return render(request, 'delete_dialog.html')


def test_json(request):
    import json
    data_string = {'test': [{'code': 0, 'content': 'this is  test'}], 'msg': 'success'}

    return render(request, 'json_result.html', {'data':json.dumps(data_string)})
