# encoding:utf-8
# @CreateTime:2023/3/14 23:04
# @Author:xuguangchun
# @FlieName:user_session.py.py
# @SoftWare:PyCharm

from django.utils.deprecation import MiddlewareMixin
from django.shortcuts import redirect


class TestMiddleware(MiddlewareMixin):
    def process_request(self, request):
        # 不用登录的页面的地址需要放行，避免直接被拦截
        if request.path_info in ['/login/', '/register/']:
            return
        user_session = request.session.get("user_session")
        # 判断用户session是否存在，否则重定向到登录页面
        if user_session:
            if request.path_info in ['/', ]:
                return redirect('/userinfo/')

            request.session_userid = user_session["id"]
            request.session_username = user_session["account"]
            print("request.session_username:", request.session_username)
            return
        else:
            return redirect('/login/')

    def process_response(self, request, response):
        # print("我走了")
        return response



